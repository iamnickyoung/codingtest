# Design Decisions
The application is a Web API with a single controller that accepts request content in the provided structure. The request data model is mapped to an internal domain model so only relevent data is referenced when passed around.

The controller is injected with a service that is responsible for creating the intended result. The service itself is injected with a Calculator service that is responsible for generating the End of Day balances for an account.

A unit test project is included that has tests covering some cases for the two services. 

One assumption I've made is that the input data allows for more than one account to be included. I've therefore altered the return object to account for this, so multiple accounts end of day balances can be returned, with the account id as the key.

If I was to expand on this solution I would add further error handling via middleware to catch exceptions and return a standard error message format. I would also add in component/integration tests for the service by utilising the WebApplicationFactory TestServer to run tests against the API endpoint. Another feature that could be added is a security layer on the endpoint by adding required JwtBear token authetnication. 