﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.Enums;

namespace AccountBalance.Api.Tests.Fixture.Models
{
    [ExcludeFromCodeCoverage]
    public static class TransactionFixture
    {
        public static Transaction GenerateTransaction(int amount, string creditDebitIndicator, int dateOffsetInDays)
        {
            return new Transaction
            {
                Amount = amount,
                CreditDebitIndicator = creditDebitIndicator,
                BookingDate = DateTime.Now.AddDays(dateOffsetInDays),
            };          
        }

        public static IList<Transaction> GenerateValidTransactions()
        {
            return new List<Transaction>
            {
                GenerateTransaction(25, CreditDebtIndicatorEnum.Credit.ToString(), -1),
                GenerateTransaction(10, CreditDebtIndicatorEnum.Credit.ToString(), -2),
                GenerateTransaction(100, CreditDebtIndicatorEnum.Debit.ToString(), -2),
                GenerateTransaction(10, CreditDebtIndicatorEnum.Credit.ToString(), -3),
            };
        }            
    }
}

