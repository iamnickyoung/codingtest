﻿using System;
using System.Collections.Generic;

using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Tests.Fixtures.Models
{
    public class FakeEndOfDayBalance : EndOfDayBalance
    {
        public FakeEndOfDayBalance(int balance, int dateOffsetInDays)
        {
            Balance = balance;
            Date = DateTime.Now.AddDays(dateOffsetInDays).ToString("dd-MM-yyyy");
        }

        public static IList<EndOfDayBalance> GenerateValidEndOfDayTransactions()
        {
            return new List<EndOfDayBalance>
            {
                new FakeEndOfDayBalance(100, -1),
                new FakeEndOfDayBalance(200, -2),
                new FakeEndOfDayBalance(300, -3)
            };
        }
    }
}
