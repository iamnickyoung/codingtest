﻿using System;
using System.Collections.Generic;

using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Tests.Fixtures.Models
{
    public static class RequestedAccountsFixture
    {
        public static RequestedAccounts Valid()
        {
            return new RequestedAccounts
            {
                Accounts = new List<Account>
                {
                   AccountFixture.Valid()
                },
                TimeStamp = DateTime.Now.AddDays(-1)
            };
        }

        public static RequestedAccounts ValidMultipleAccounts()
        {
            var fakeRequest = Valid();
            fakeRequest.Accounts.Add(AccountFixture.Valid("23456"));
            return fakeRequest;
        }

        public static RequestedAccounts EmptyAccounts()
        {
            return new RequestedAccounts
            {
                Accounts = new List<Account>()
            };
        }

        public static RequestedAccounts NullAccounntId()
        {
            var fakeRequest = Valid();
            fakeRequest.Accounts[0].AccountId = null;
            return fakeRequest;
        }
    }
}
