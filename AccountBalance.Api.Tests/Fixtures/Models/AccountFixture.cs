﻿using System.Collections.Generic;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Tests.Fixture.Models;

namespace AccountBalance.Api.Tests.Fixtures.Models
{
    public static class AccountFixture
    {
        public static Account Valid(string id = null)
        {
            return new Account
            {
                Transactions = TransactionFixture.GenerateValidTransactions(),
                Balances = new Balances
                {
                    Current = new Current
                    {
                        Amount = 100
                    }
                },
                AccountId = id ?? "12345"
            };
        }

        public static Account EmptyTransactions()
        {
            return new Account
            {
                Transactions = new List<Transaction>(),
                Balances = new Balances
                {
                    Current = new Current()
                }
            };
        }

        public static Account NullBalances()
        {
            return new Account
            {
                Transactions = new List<Transaction>()  
            };
        }

        public static Account EmptyBalances()
        {
            return new Account
            {
                Transactions = new List<Transaction>(),
                Balances = new Balances()
            };
        }
    }
}
