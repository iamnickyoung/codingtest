﻿using System;
using System.Collections.Generic;
using System.Linq;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.DTOs.Responses;
using AccountBalance.Api.Services;
using AccountBalance.Api.Services.Calculators;
using AccountBalance.Api.Tests.Fixtures.Models;

using FakeItEasy;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AccountBalance.Api.Tests.Services
{
    [TestClass]
    public class AccountBalanceServiceShould
    {
        private AccountBalanceService _service;
        private IBalanceCalculator _calculator;

        [TestInitialize]
        public void SetUp()
        {
            _calculator = A.Fake<IBalanceCalculator>();
            _service = new AccountBalanceService(_calculator);
        }

        [TestCleanup]
        public void TearDown()
        {
            _calculator = null;
            _service = null;
        }

        [TestMethod]
        public void Return_TotalBalances()
        {
            var fakeRequest = RequestedAccountsFixture.Valid();
            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).Returns(FakeEndOfDayBalance.GenerateValidEndOfDayTransactions());

            var result = _service.CalculateBalance(fakeRequest);

            Assert.IsInstanceOfType(result, typeof(IDictionary<string, TotalBalances>));
        }

        [TestMethod]
        public void Return_TotalBalancesWithMatchingKey()
        {
            var fakeRequest = RequestedAccountsFixture.Valid();
            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).Returns(FakeEndOfDayBalance.GenerateValidEndOfDayTransactions());

            var result = _service.CalculateBalance(fakeRequest);

            Assert.IsTrue(result.ContainsKey(fakeRequest.Accounts[0].AccountId));
        }

        [TestMethod]
        public void Call_BalanceCalculatorOnce()
        {
            var fakeRequest = RequestedAccountsFixture.Valid();
            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).Returns(FakeEndOfDayBalance.GenerateValidEndOfDayTransactions());

            _service.CalculateBalance(fakeRequest);

            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void Call_BalanceCalculatorMultipleTimes()
        {
            var fakeRequest = RequestedAccountsFixture.ValidMultipleAccounts();
            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).Returns(FakeEndOfDayBalance.GenerateValidEndOfDayTransactions());

            _service.CalculateBalance(fakeRequest);

            A.CallTo(() => _calculator.Calculate(A<Account>._, A<DateTime>._)).MustHaveHappenedTwiceExactly();
        }

        [TestMethod]
        public void Return_EmptyDictionary_When_ArgumentAccountsPropertyIsEmpty()
        {
            var fakeRequest = RequestedAccountsFixture.EmptyAccounts();
            var result = _service.CalculateBalance(new RequestedAccounts { Accounts = new List<Account>() });

            Assert.IsInstanceOfType(result, typeof(IDictionary<string, TotalBalances>));
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowException_WhenArgumentIsNull()
        {
            _service.CalculateBalance(null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowException_When_ArgumentAccountsPropertyIsNull()
        {
            _service.CalculateBalance(new RequestedAccounts());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowException_When_ArgumentAccountIdPropertyIsNull()
        {
            _service.CalculateBalance(new RequestedAccounts { Accounts = new List<Account> { new Account { Transactions = new List<Transaction>() }}});
        }
    }
}
