﻿using System;
using System.Collections.Generic;
using System.Linq;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Services.Calculators;
using AccountBalance.Api.Tests.Fixture.Models;
using AccountBalance.Api.Tests.Fixtures.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AccountBalance.Api.Tests.Services.Calculators
{
    [TestClass]
    public class BalanceCalculatorShould
    {
        private BalanceCalculator _calculator;

        [TestInitialize]
        public void SetUp()
        {
            _calculator = new BalanceCalculator();
        }

        [TestCleanup]
        public void TearDown()
        {
            _calculator = null;
        }

        [TestMethod]
        public void Return_EndOfDayBalances()
        {
            var fakeAccount = AccountFixture.Valid();

            var result = _calculator.Calculate(fakeAccount, DateTime.Now);

            Assert.IsInstanceOfType(result, typeof(IList<EndOfDayBalance>));
        }

        [TestMethod]
        public void Return_EmptyList_When_ArgumentTransactionsPropertyIsEmpty()
        {
            var fakeAccount = AccountFixture.EmptyTransactions();

            var result = _calculator.Calculate(fakeAccount, DateTime.Now);

            Assert.IsTrue(result.Count == 0);
        }

        [TestMethod]
        public void Return_CorrectNumberOfBalanceResults()
        {
            var fakeRequest = AccountFixture.Valid();
            var transactionCount = fakeRequest.Transactions.GroupBy(x => x.BookingDate.Date).Count();

            var result = _calculator.Calculate(fakeRequest, DateTime.Now);

            Assert.AreEqual(transactionCount, result.Count);
        }        

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowException_When_ArgumentIsNull()
        {
            _calculator.Calculate(null, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowException_When_ArgumentTransactionsPropertyIsNull()
        {
            var fakeAccount = new Account();
            _calculator.Calculate(fakeAccount, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowException_When_ArgumentBalancesPropertyIsNull()
        {
            var fakeAccount = AccountFixture.NullBalances();
            _calculator.Calculate(fakeAccount, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ThrowException_When_ArgumentBalancesCurrentPropertyIsNull()
        {
            var fakeAccount = AccountFixture.EmptyBalances();
            _calculator.Calculate(fakeAccount, DateTime.Now);
        }
    }
}
