﻿using AccountBalance.Api.Models;
using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.DTOs.Requests;
using AutoMapper;

namespace AccountBalance.Api.Mappings
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<RequestedAccounts, AccountBalanceRequestDto>()
                .ForMember(dest => dest.RequestDateTime,
                    opts => opts.MapFrom(
                        src => src.TimeStamp))
                .ReverseMap();

            CreateMap<Account, AccountDto>().ReverseMap();
            CreateMap<Balances, BalancesDto>().ReverseMap();
            CreateMap<Current, CurrentDto>().ReverseMap();
            CreateMap<Available, AvailableDto>().ReverseMap();
            CreateMap<Transaction, TransactionDto>().ReverseMap();
        }
    }
}
