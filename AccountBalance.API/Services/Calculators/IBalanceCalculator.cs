﻿using System;
using System.Collections.Generic;

using AccountBalance.Api.Models;
using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Services.Calculators
{
    public interface IBalanceCalculator
    {
        IList<EndOfDayBalance> Calculate(Account account, DateTime timeStampe);
    }
}
