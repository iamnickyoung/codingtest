﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountBalance.Api.Extensions;
using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Services.Calculators
{
    public class BalanceCalculator : IBalanceCalculator
    {
        public IList<EndOfDayBalance> Calculate(Account account, DateTime timeStamp)
        {
            if (account == null)
            {
                throw new ArgumentNullException($"{nameof(account)} cannot be null");
            }

            if(account.Transactions == null)
            {
                throw new InvalidOperationException($"{nameof(account.Transactions)} cannot be null");
            }

            var groupedTransactions = account.Transactions.GroupByDate();
            groupedTransactions = CheckCurrentDayTransactions(groupedTransactions, timeStamp);
            return GenerateEndOfDayBalances(account, groupedTransactions);
        }

        private IList<EndOfDayBalance> GenerateEndOfDayBalances(
            Account account, 
            List<KeyValuePair<DateTime, List<Transaction>>> groupedTransactions)
        {
            if (account.Balances == null)
            {
                throw new InvalidOperationException($"{nameof(account.Balances)} cannot be null");
            }

            if (account.Balances.Current == null)
            {
                throw new InvalidOperationException($"{nameof(account.Balances.Current)} cannot be null");
            }

            var currentBalance = account.Balances.Current.Amount;
            var endOfDayBalances = new List<EndOfDayBalance>();
            for (var i = 0; i < groupedTransactions.Count - 1; i++)
            {
                currentBalance = GenerateEndOfDayBalance(currentBalance, groupedTransactions[i]);
                var endOfDayBalance = new EndOfDayBalance
                {
                    Balance = currentBalance,
                    Date = groupedTransactions[i+1].Key.ToString("yyyy-MM-dd"),
                };
                endOfDayBalances.Add(endOfDayBalance);
            }

            return endOfDayBalances;
        }

        private List<KeyValuePair<DateTime, List<Transaction>>> CheckCurrentDayTransactions(
            List<KeyValuePair<DateTime, List<Transaction>>> groupedTransactions, 
            DateTime timeStamp)
        {
            if (groupedTransactions.FirstOrDefault().Key != timeStamp.Date)
            {
                groupedTransactions.Add(new KeyValuePair<DateTime, List<Transaction>>(timeStamp.Date, new List<Transaction>()));
                groupedTransactions = groupedTransactions.OrderByDescending(x => x.Key.Date).ToList();
            }

            return groupedTransactions;
        }

        private int GenerateEndOfDayBalance(
            int currentBalance, 
            KeyValuePair<DateTime, List<Transaction>> datedTransactions)
        {          
            foreach (var transaction in datedTransactions.Value)
            {                
                if (transaction.IsCredit())
                {
                    currentBalance -= transaction.Amount;
                }

                if(transaction.IsDebit())
                {
                    currentBalance += transaction.Amount;
                }                
            }

            return currentBalance;
        }
    }
}