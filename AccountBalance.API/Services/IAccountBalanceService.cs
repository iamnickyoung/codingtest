﻿using System.Collections.Generic;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.DTOs.Responses;

namespace AccountBalance.Api.Services
{
    public interface IAccountBalanceService
    {
        IDictionary<string, TotalBalances> CalculateBalance(RequestedAccounts accountsRequest);
    }
}
