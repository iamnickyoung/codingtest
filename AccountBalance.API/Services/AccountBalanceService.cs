﻿using System;
using System.Collections.Generic;
using System.Linq;

using AccountBalance.Api.Extensions;
using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.DTOs.Responses;
using AccountBalance.Api.Services.Calculators;

namespace AccountBalance.Api.Services
{
    public class AccountBalanceService : IAccountBalanceService
    {
        private readonly IBalanceCalculator _calculator;

        public AccountBalanceService(IBalanceCalculator calculator)
        {
            _calculator = calculator;
        }

        public IDictionary<string, TotalBalances> CalculateBalance(RequestedAccounts requestedAccounts)
        {
            if(requestedAccounts == null)
            {
                throw new ArgumentNullException($"{nameof(requestedAccounts)} cannot be null");
            }

            if(requestedAccounts.Accounts == null)
            {
                throw new InvalidOperationException($"{nameof(requestedAccounts.Accounts)} cannot be null");
            }

            if(requestedAccounts.Accounts.Any(x => x.AccountId == null))
            {
                throw new InvalidOperationException("Account IDs cannot be null");
            }

            var accountBalances = new Dictionary<string, TotalBalances>();
            foreach(var account in requestedAccounts.Accounts)
            {
                var endOfDayBalances = _calculator.Calculate(account, requestedAccounts.TimeStamp);
                var totalBalances = GenerateTotalBalances(endOfDayBalances, account.Transactions);
                accountBalances.Add(account.AccountId, totalBalances);
            }

            return accountBalances;
        }

        private TotalBalances GenerateTotalBalances(IList<EndOfDayBalance> endOfDayBalances, IList<Transaction> transactions)
        {
            return new TotalBalances
            {
                TotalCredits = transactions.CountCreditIndicators(),
                TotalDebits = transactions.CountDebitIndicators(),
                EndOfDayBalances = endOfDayBalances
                                   .OrderByDescending(x => x.Date)
                                   .ToList()
            };
        }
    }
}
