﻿using AccountBalance.Api.Services;
using Microsoft.AspNetCore.Mvc;
using AccountBalance.Api.Filters;
using AccountBalance.Api.Models.DTOs.Requests;
using AccountBalance.Api.Models;
using AutoMapper;
using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AccountBalanceController : ControllerBase
    {
        private readonly IAccountBalanceService _accountBalanceService;
        private readonly IMapper _mapper;

        public AccountBalanceController(IAccountBalanceService accountBalanceService, IMapper mapper)
        {
            _accountBalanceService = accountBalanceService;
            _mapper = mapper;
        }

        [HttpPost]
        [ValidateModel]
        public IActionResult Post(AccountBalanceRequestDto request)
        {
            var accountsModel = _mapper.Map<RequestedAccounts>(request);

            var balanceResult = _accountBalanceService.CalculateBalance(accountsModel);

            return new JsonResult(balanceResult);
        }
    }
}