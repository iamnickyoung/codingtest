﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using AccountBalance.Api.Models.Domain;
using AccountBalance.Api.Models.Enums;

namespace AccountBalance.Api.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class TransactionExtensions
    {
        public static List<KeyValuePair<DateTime, List<Transaction>>> GroupByDate(this IEnumerable<Transaction> transactions)
        {
            return transactions
                .GroupBy(x => x.BookingDate.Date)
                .ToDictionary(x => x.Key, x => x.ToList())
                .OrderByDescending(x => x.Key.Date)
                .ToList();
        }

        public static bool IsCredit(this Transaction transaction)
        {
            return transaction.CreditDebitIndicator.Equals(
                CreditDebtIndicatorEnum.Credit.ToString(), 
                StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsDebit(this Transaction transaction)
        {
            return transaction.CreditDebitIndicator.Equals(
                CreditDebtIndicatorEnum.Debit.ToString(),
                StringComparison.InvariantCultureIgnoreCase);
        }

        public static int CountCreditIndicators(this IEnumerable<Transaction> transaction)
        {
            return transaction.Count(x => x.IsCredit());

        }

        public static int CountDebitIndicators(this IEnumerable<Transaction> transaction)
        {
            return transaction.Count(x => x.IsDebit());
        }
    }
}
