﻿namespace AccountBalance.Api.Models.Enums
{
    public enum CreditDebtIndicatorEnum
    {
        Credit,
        Debit
    }
}
