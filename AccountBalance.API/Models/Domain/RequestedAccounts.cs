﻿using System;
using System.Collections.Generic;

namespace AccountBalance.Api.Models.Domain
{
    public class RequestedAccounts
    {
        public DateTime TimeStamp { get; set; }

        public IList<Account> Accounts { get; set; }
    }
}
