﻿namespace AccountBalance.Api.Models.Domain
{
    public class Available
    {
        public int Amount { get; set; }
        public string CreditDebitIndicator { get; set; }
        public object CreditLines { get; set; }
    }
}
