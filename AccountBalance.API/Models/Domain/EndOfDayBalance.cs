﻿namespace AccountBalance.Api.Models.Domain
{
    public class EndOfDayBalance
    {
        public string Date { get; set; }
        public int Balance { get; set; }
    }
}