﻿using System;

namespace AccountBalance.Api.Models.Domain
{
    public class Transaction
    {
        public string Description { get; set; }
        public int Amount { get; set; }
        public string CreditDebitIndicator { get; set; }
        public string Status { get; set; }
        public DateTime BookingDate { get; set; }
        public object MerchantDetails { get; set; }
    }
}
