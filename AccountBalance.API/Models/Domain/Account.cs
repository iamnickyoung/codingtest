﻿using System.Collections.Generic;

namespace AccountBalance.Api.Models.Domain
{
    public class Account
    {
        public string AccountId { get; set; }
        public Balances Balances { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }
}
