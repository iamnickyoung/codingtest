﻿using System.Collections.Generic;

using AccountBalance.Api.Models.Domain;

namespace AccountBalance.Api.Models.DTOs.Responses
{
    public class TotalBalances
    {
        public int TotalCredits { get; set; }
        public int TotalDebits { get; set; }
        public IList<EndOfDayBalance> EndOfDayBalances { get; set; }
    }
}
