﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountBalance.Api.Models.DTOs.Requests
{
    public class AccountDto
    {
        public string AccountId { get; set; }
        public string CurrencyCode { get; set; }
        public string DisplayName { get; set; }
        public string AccountType { get; set; }
        public string AccountSubType { get; set; }
        public IdentifiersDto Identifiers { get; set; }
        public IList<object> Parties { get; set; }
        public IList<object> StandingOrders { get; set; }
        public IList<object> DirectDebits { get; set; }
        [Required]
        public BalancesDto Balances { get; set; }
        [Required]
        public IList<TransactionDto> Transactions { get; set; }
    }
}
