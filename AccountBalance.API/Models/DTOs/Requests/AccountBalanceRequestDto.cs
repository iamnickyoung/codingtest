﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountBalance.Api.Models.DTOs.Requests
{
    public class AccountBalanceRequestDto
    {
        public string BrandName { get; set; }
        public string DataSourceName { get; set; }
        public string DataSourceType { get; set; }
        [Required]
        public DateTime RequestDateTime { get; set; }
        [Required]
        public IList<AccountDto> Accounts { get; set; }
    }
}
