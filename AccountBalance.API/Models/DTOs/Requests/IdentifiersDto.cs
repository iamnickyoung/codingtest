﻿namespace AccountBalance.Api.Models.DTOs.Requests
{
    public class IdentifiersDto
    {
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public object SecondaryIdentification { get; set; }
    }
}
