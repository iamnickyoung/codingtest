﻿namespace AccountBalance.Api.Models.DTOs.Requests
{
    public class BalancesDto
    {
        public CurrentDto Current { get; set; }
        public AvailableDto Available { get; set; }
    }
}
