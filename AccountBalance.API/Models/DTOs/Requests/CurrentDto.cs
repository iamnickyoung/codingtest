﻿namespace AccountBalance.Api.Models.DTOs.Requests
{
    public class CurrentDto
    {
        public int Amount { get; set; }
        public string CreditDebitIndicator { get; set; }
        public object CreditLines { get; set; }
    }
}